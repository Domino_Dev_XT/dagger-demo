package com.domino.dagger.demo.di.component;

import com.domino.dagger.demo.di.module.AnimalModule;

import dagger.Subcomponent;

/**
 * Created by kanejin on 15/05/2017.
 */
@Subcomponent(modules = {
        AnimalModule.class
})
public interface AnimalComponent {

}
