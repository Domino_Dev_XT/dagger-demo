package com.domino.dagger.demo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity {

    @Inject GameControler gameControler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DemoApplication.getComponent().inject(this);

        gameControler.onStart();
    }

    public void randomMatch(View v){
        gameControler.randomMatch();
    }
}
