package com.domino.dagger.demo.di.component;

import com.domino.dagger.demo.di.module.ActivityModule;
import com.domino.dagger.demo.di.scope.ActivityScope;

import dagger.Subcomponent;

/**
 * Created by kanejin on 15/05/2017.
 */
@ActivityScope
@Subcomponent(modules = {
        ActivityModule.class
})
public interface ActivityComponent {
}
