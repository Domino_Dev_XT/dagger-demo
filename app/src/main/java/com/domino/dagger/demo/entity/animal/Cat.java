package com.domino.dagger.demo.entity.animal;

/**
 * Created by kanejin on 15/05/2017.
 */

public class Cat extends Animal {

    @Override
    public boolean isGrownUp() {
        return age >= 3;
    }
}
