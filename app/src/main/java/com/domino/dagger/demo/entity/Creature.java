package com.domino.dagger.demo.entity;

/**
 * Created by kanejin on 15/05/2017.
 */

public interface Creature {

    boolean isAlive();
    boolean isGrownUp();

}
