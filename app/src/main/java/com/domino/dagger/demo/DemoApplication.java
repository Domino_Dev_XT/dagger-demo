package com.domino.dagger.demo;

import android.app.Application;

import com.domino.dagger.demo.di.component.ApplicationComponent;
import com.domino.dagger.demo.di.component.DaggerApplicationComponent;
import com.domino.dagger.demo.di.component.DaggerHumanComponent;
import com.domino.dagger.demo.di.component.HumanComponent;
import com.domino.dagger.demo.di.module.ApplicationModule;

/**
 * Created by kanejin on 15/05/2017.
 */

public class DemoApplication extends Application {

    private static DemoApplication instance;

    private static ApplicationComponent component;

    private static HumanComponent playerComponent;
    //private static HumanComponent player2Component;
    private static HumanComponent comComponent;

    public static DemoApplication getInstance(){
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        initializeDagger();
    }

    private void initializeDagger(){
        component = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        playerComponent = DaggerHumanComponent.builder().build();
        comComponent = DaggerHumanComponent.builder().build();
    }

    public static ApplicationComponent getComponent() {
        return component;
    }

    public static HumanComponent getPlayerComponent(){
        return playerComponent;
    }

    public static HumanComponent getComComponent(){
        return comComponent;
    }
}
