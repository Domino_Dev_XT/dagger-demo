package com.domino.dagger.demo.entity.human;

import com.domino.dagger.demo.entity.IGame;

/**
 * Created by kanejin on 15/05/2017.
 */

public abstract class Human implements HumanAction {

    private int age = 1;
    private float hp = 100.0f;
    private int AGE_OF_GROWN_UP = 16;

    @Override
    public boolean isAlive() {
        return hp > 0.0f;
    }

    @Override
    public boolean isGrownUp() {
        return age >= AGE_OF_GROWN_UP;
    }
}
