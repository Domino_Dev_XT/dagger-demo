package com.domino.dagger.demo.entity;

/**
 * Created by kanejin on 15/05/2017.
 */

public interface IGame {

    enum Action {
        Scissors("剪刀", 0), Rock("石头", 1), Fabric("布", 2);

        private String actionName;
        private int value;

        Action(String actionName, int value){
            this.actionName = actionName;
            this.value = value;
        }

        public String getActionName(){
            return actionName;
        }

        public int getValue(){
            return value;
        }
    }

    enum Result{
        Win("赢"), Dogfall("平"), Lose("输");

        private String resultName;

        Result(String resultName){
            this.resultName = resultName;
        }

        public String getResultName(){
            return resultName;
        }
    }

    void onStart();
    void onPause();
    void onContinue();
    void onVictory();
    void onFailure();
    Result match(Action action1, Action action2);

}
