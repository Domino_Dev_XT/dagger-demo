package com.domino.dagger.demo.di.module;

import android.content.Context;

import com.domino.dagger.demo.DemoApplication;
import com.domino.dagger.demo.GameControler;
import com.domino.dagger.demo.di.scope.ApplicationScope;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by kanejin on 15/05/2017.
 */

@Module
public class ApplicationModule {

    private DemoApplication application;

    public ApplicationModule(DemoApplication application){
        this.application = application;
    }

    @Provides
    @Singleton
    Context providesContext(){
        return application.getApplicationContext();
    }

    @Provides
    @Singleton
    GameControler providesGameControler(@ApplicationScope Context context){
        return new GameControler(context);
    }
}
