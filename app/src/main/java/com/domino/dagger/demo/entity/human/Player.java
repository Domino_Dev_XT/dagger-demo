package com.domino.dagger.demo.entity.human;

import com.domino.dagger.demo.entity.IGame;

/**
 * Created by kanejin on 15/05/2017.
 */

public class Player extends Human {

    private String name;

    public Player(String name){
        this.name = name;
    }

    @Override
    public boolean isAlive() {
        return false;
    }

    @Override
    public boolean isGrownUp() {
        return false;
    }

    @Override
    public void sayHello() {
        System.out.println("Hello, I'm " + name);
    }

    public IGame.Action randomAction(){
        IGame.Action randomAction;
        int actionValue = (int) (Math.random() * 3);

        if(actionValue == IGame.Action.Scissors.getValue()){
            randomAction = IGame.Action.Scissors;
        }
        else if(actionValue == IGame.Action.Rock.getValue()){
            randomAction = IGame.Action.Rock;
        }
        else {
            randomAction = IGame.Action.Fabric;
        }
        return randomAction;
    }

}
