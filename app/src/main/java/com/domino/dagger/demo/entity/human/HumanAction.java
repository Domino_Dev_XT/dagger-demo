package com.domino.dagger.demo.entity.human;

import com.domino.dagger.demo.entity.Creature;

/**
 * Created by kanejin on 15/05/2017.
 */

public interface HumanAction extends Creature{

    void sayHello();
}
