package com.domino.dagger.demo.di.component;

import com.domino.dagger.demo.di.module.AnimalModule;
import com.domino.dagger.demo.di.module.HumanModule;

import dagger.Component;

/**
 * Created by kanejin on 15/05/2017.
 */
@Component(modules = {
        HumanModule.class
})
public interface HumanComponent {

    AnimalComponent plus(AnimalModule module);
}
