package com.domino.dagger.demo.di.component;

import com.domino.dagger.demo.MainActivity;
import com.domino.dagger.demo.di.module.ActivityModule;
import com.domino.dagger.demo.di.module.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by kanejin on 15/05/2017.
 */
@Component(modules = {
        ApplicationModule.class
})
@Singleton
public interface ApplicationComponent {

    ActivityComponent plus(ActivityModule module);

    void inject(MainActivity activity);

}
