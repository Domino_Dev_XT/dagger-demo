package com.domino.dagger.demo.di.module;

import android.app.Activity;

import com.domino.dagger.demo.di.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

/**
 * Created by kanejin on 15/05/2017.
 */
@Module
public class ActivityModule {

    private Activity activity;

    public ActivityModule(Activity activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    Activity providesActivity() {
        return activity;
    }
}
