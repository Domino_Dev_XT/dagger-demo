package com.domino.dagger.demo;

import android.content.Context;

import com.domino.dagger.demo.di.scope.ApplicationScope;
import com.domino.dagger.demo.entity.IGame;
import com.domino.dagger.demo.entity.human.Player;

/**
 * Created by kanejin on 16/05/2017.
 */

public class GameControler implements IGame {

    private Player player, com;

    public GameControler(@ApplicationScope Context context){
        player = new Player("Player");
        com = new Player("Computer");
    }

    @Override
    public void onStart() {
        System.out.print("Start Game");
    }

    @Override
    public void onPause() {
        System.out.print("Pause Game");
    }

    @Override
    public void onContinue() {
        System.out.print("Continue Game");
    }

    @Override
    public void onVictory() {
        System.out.print("You Win");
    }

    @Override
    public void onFailure() {
        System.out.print("You Lose");
    }

    @Override
    public Result match(Action action1, Action action2) {
        int delta = action1.getValue() - action2.getValue();
        Result result;
        if(0 == delta){
            result = Result.Dogfall;
        }
        else if(-1 == delta){
            result = Result.Lose;
        }
        else {
            result = Result.Win;
        }
        return result;
    }

    public void randomMatch(){
        Result result = match(player.randomAction(), com.randomAction());
        System.out.println("结果：" + result.getResultName());
    }

}
