package com.domino.dagger.demo.entity.animal;

/**
 * Created by kanejin on 15/05/2017.
 */

public abstract class Animal implements AnimalAction {

    protected int age;
    protected float hp;

    @Override
    public boolean isAlive() {
        return hp >= 100.0f;
    }

}
